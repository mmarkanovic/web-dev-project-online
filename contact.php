<?php
ob_start();
session_start();
include_once 'header.php'
?>
<head>
    <link rel="stylesheet" href="css/contactStyle.css">
</head>
<body>
    <div class="container">
        <main>
            <p>SEND E-MAIL</p>
            <form class="contact-form" action="contact/contactform.php" method="post">
                <input type="text" name="contName" placeholder="Full name">
                <input type="text" name="contMail" placeholder="Your e-mail">
                <input type="text" name="contSubject" placeholder="Subject">
                <textarea name="contMessage" id="" cols="30" rows="10" placeholder="Message"></textarea>
                <button type="submit" name="submit" id="sendMail">SEND MAIL</button>
            </form>
        </main> 
    </div>
</body>
</html>