<?php
ob_start();
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="logStyle.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   
    <title>Login</title>
</head>
<body>
<div class="container">
  <form action=" ../includes/logininc.php" method="POST">
    <div class="row">
      <h2 style="text-align:center">Login</h2>
        <div class="col1">
            <input type="text" name="uname" placeholder="Username/e-mail" required>
            <input type="password" name="pass" placeholder="Password" required>
            <input type="submit" name="submit" value="Login">
        </div>
      </div>

    
  </form>


    <div id="bottom-container">
        <div class="row">
            <div class="col">
                <a href=" ../reg/signUp.php" style="color:white" class="btn">Sign up</a>
            </div>
        </div>
    </div>


</div>
</body>
</html>