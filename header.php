<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/contentStyle.css">
    <link rel="stylesheet" href="css/sidebarStyle.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   
    <title>13MM</title>
</head>
<body>
    <header>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href=""></a>
                </div>
                <?php
                if (isset($_SESSION['u_id'])){
                    echo '<ul class="nav navbar-nav">
                            <li><a href="hot.php">Home</a></li>
                            <li><a href="content/addcont.php">Add content</a></li>
                            <li><a href="contact.php">Contact us</a></li>
                        </ul>';
                } else{
                    echo '<ul class="nav navbar-nav">
                            <li><a href="hot.php">Home</a></li>
                            <li><a href="#">Contact us</a></li>
                        </ul>';
                }
                ?>
                <?php
                    if (isset($_SESSION['u_id'])) {
                      echo '<form action="includes/logout.inc.php" method="POST"> 
                                <button type="submit" name="submit" class="btn btn-danger navbar-btn">Logout</button>                
                            </form>';  
                    }else {
                       echo '<div class="nav navbar-nav navbar-right">
                                <a href="login/login.php"><button class="btn btn-default navbar-btn ">Log in</button></a>
                                <a href="reg/signUp.php"><button class="btn btn-danger navbar-btn">Sign up</button></a>
                            </div>';
                    }
                ?>
               
                
            </div>
        </nav>
    </header>
</body>
</html>