<?

session_start();

if(isset($_POST['submit'])){
    include 'dbh.inc.php';

    $uname= mysqli_real_escape_string($conn, $_POST['uname'];
    $pass= mysqli_real_escape_string($conn, $_POST['pass'];

    //error handlers
    //check if inputs are empty

    if (empty($uname) || empty($pass)){
        header("Location: ../index.php?login=empty");
        exit();
    } else{
        $sql= "SELECT * FROM users WHERE user_uname='$uname'";
        $result= mtsqli_query($conn, $sql);
        $recultCheck= mysqli_num_rows($result);
        if ($recultCheck < 1) {
            header("Location: ../index.php?login=error");
            exit();
        } else {
            if ($row = mysqli_fetch_assoc($result)) {
                //De-hasning the password
                $hashedPassCheck = password_verify($pass, $row['user_pass']);
                if ($hashedPassCheck == false){
                    header("Location: ../index.php?login=error");
                    exit();
                } elseif ($hashedPassCheck==true) {
                    //Log in the user here
                    $_SESSION['u_id']=$row['user_id'];
                    $_SESSION['u_name']=$row['user_name'];
                    $_SESSION['u_email']=$row['user_email'];
                    $_SESSION['u_uname']=$row['user_uname'];
                    header("Location: ../index.php?login=success");
                    exit();
                }
            }    
        }

    }

} else {
        header("Location: ../index.php?login=error");
        exit();
}


?>