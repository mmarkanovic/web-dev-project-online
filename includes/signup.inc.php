<?php
ob_start();
session_start();
include_once 'dbh.inc.php';
if (isset($_POST['submit'])){

  

    $name = mysqli_real_escape_string($conn, $_POST['name']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $uname = mysqli_real_escape_string($conn, $_POST['uname']);
    $pass = mysqli_real_escape_string($conn, $_POST['pass']);

    //error handlers
    //Check for empty fields

    if (empty($name) || empty($email) || empty($uname) || empty($pass)) {
        header("Location: ../reg/signUp.php?signUp=empty");
        exit();
    } else {
        //Check if input character are valid
        if (!preg_match("/^[a-zA-Z]*$/", $name)) {
            header("Location: ../reg/signUp.php?signUp=invalid");
            exit();
        } else {
            //Check if email is valid
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                header("Location: ../reg/signUp.php?signUp=email");
                exit();
            } else {
                $sql = "SELECT * FROM users WHERE user_uname='$uname'";
                $result = mysqli_query($conn, $sql);
                $resultCheck = mysqli_num_rows($result);

                if ($resultCheck > 0) {
                    header("Location: ../reg/signUp.php?signUp=usernametaken");
                    exit();
                } else {
                    //Hashing the password
                    $hashedPass= password_hash($pass, PASSWORD_DEFAULT);
                    //Insert the user into the database
                    $sql = "INSERT INTO users (user_name, user_email, user_uname, user_pass ) VALUES ('$name','$email','$uname','$hashedPass');";

                    mysqli_query($conn, $sql);
                    header("Location: ../hot.php?signUp=success");
                    exit();
                }
            
            }
        }
    }
    

} else {
    header("Location: ../reg/signUp.php");
    exit();
}

?>